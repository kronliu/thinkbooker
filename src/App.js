import { BrowserRouter, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import "./assets/css/global.css";
import "font-awesome/css/font-awesome.css";

import Home from "./pages/Home";
import Profile from "./pages/Profile";
import Courses from "./pages/Courses";
import Course from "./pages/Course";
import Logout from "./pages/Logout";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route
          exact
          path="/profile"
          render={(props) => <Profile {...props} />}
        />
        <Route
          exact
          path="/courses"
          render={(props) => <Courses {...props} />}
        />
        <Route path="/course" render={(props) => <Course {...props} />} />
        <Route path="/logout" render={(props) => <Logout {...props} />} />
        <Route exact path="/" render={(props) => <Home {...props} />} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
