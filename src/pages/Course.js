import React, { Component } from "react";
import NavBar from "../components/NavBar";
import { Alert } from "react-bootstrap";
import Swal from "sweetalert2";

class Course extends Component {
  state = {
    name: "",
    description: "",
    price: 0,
    users: [],
    loading: true,
  };

  componentDidMount() {
    let params = new URLSearchParams(window.location.search);
    let courseId = params.get("courseId");
    fetch(`https://intense-waters-48513.herokuapp.com/api/courses/${courseId}`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        const name = data.name,
          description = data.description,
          price = data.price;

        this.setState({ name, description, price });
        this.setState({ loading: false });
        data.enrollees.forEach((user) => {
          fetch(
            `https://intense-waters-48513.herokuapp.com/api/users/enrollees/${user.userId}`
          )
            .then((res) => {
              return res.json();
            })
            .then((data) => {
              let users = [...this.state.users];
              users.push(data);
              this.setState({ users });
            });
        });
      });
  }

  handleEnroll = () => {
    let params = new URLSearchParams(window.location.search);
    let courseId = params.get("courseId");
    fetch("https://intense-waters-48513.herokuapp.com/api/users/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        const userId = data._id;
        fetch("https://intense-waters-48513.herokuapp.com/api/users/enroll", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({
            userId: userId,
            courseId: courseId,
          }),
        })
          .then((res) => {
            return res.json();
          })
          .then((data) => {
            if (data) {
              Swal.fire({
                icon: "success",
                title: "You have Enrolled Successfully",
              });
              fetch(
                `https://intense-waters-48513.herokuapp.com/api/courses/${courseId}`
              )
                .then((res) => {
                  return res.json();
                })
                .then((data) => {
                  const name = data.name,
                    description = data.description,
                    price = data.price;

                  this.setState({ name, description, price });
                  this.setState({ loading: false });
                  data.enrollees.forEach((user) => {
                    fetch(
                      `https://intense-waters-48513.herokuapp.com/api/users/enrollees/${user.userId}`
                    )
                      .then((res) => {
                        return res.json();
                      })
                      .then((data) => {
                        let users = [...this.state.users];
                        users.push(data);
                        this.setState({ users });
                      });
                  });
                });
            } else {
              Swal.fire({
                icon: "error",
                title: "Enrollment Failed",
              });
            }
          });
      });
  };

  render() {
    if (this.state.loading) {
      return (
        <div className="loading-screen d-flex flex-column align-items-center justify-content-center background-img">
          <div className="loader"></div>
        </div>
      );
    }
    return (
      <>
        <NavBar />
        <div className="gradient-divider"></div>
        <header className="text-center d-flex flex-column align-items-center justify-content-center background-img">
          <h2 className="my-4 text-white">{this.state.name}</h2>
          <p className="lead text-white">{`Course Description: ${this.state.description}`}</p>
          <p className="lead text-white">{`Price: ${this.state.price}.00 PHP`}</p>
        </header>
        <div className="divider shadow-sm">
          {localStorage.getItem("isAdmin") === "false" && (
            <button
              className="btn btn-primary"
              id="createCourseBtn"
              onClick={this.handleEnroll}
            >
              Enroll
            </button>
          )}
        </div>
        <section className="bg-grey py-5">
          <div className="container">
            {this.state.users.length === 0 && (
              <Alert variant="info" className="alert text-center">
                <Alert.Heading>No Enrollees.</Alert.Heading>
              </Alert>
            )}
            {localStorage.getItem("isAdmin") !== null ? (
              <>
                <h3 className="text-center">Enrollees:</h3>
                <hr className="my-4" />
                <div className="row">
                  {this.state.users.map((user) => {
                    return (
                      <div className="col-12 col-md-6" key={user._id}>
                        <div className="card shadow container text-center my-2">
                          <div className="row flex flex-column p-3">
                            <h4 className="course-name text-dark">{`${user.firstname} ${user.lastname}`}</h4>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </>
            ) : (
              <Alert variant="info" className="alert text-center">
                <Alert.Heading>You must be logged in to enroll.</Alert.Heading>
              </Alert>
            )}
          </div>
        </section>
      </>
    );
  }
}

export default Course;
