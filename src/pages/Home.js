import React, { Component } from "react";
import NavBar from "../components/NavBar";

import { Redirect } from "react-router-dom";

class Home extends Component {
  state = {
    willRedirect: false,
  };
  handleRedirect = () => {
    this.setState({ willRedirect: true });
  };

  render() {
    return this.state.willRedirect ? (
      <Redirect
        to={
          localStorage.getItem("isAdmin") === "false" ? "/profile" : "/courses"
        }
      />
    ) : (
      <>
        <NavBar handleRedirect={this.handleRedirect} />

        <header className="text-center d-flex flex-column align-items-center justify-content-center background-img">
          <h1 className="text-white">ThinkBooker Booking Services</h1>
          <p className="lead text-white">Booking for everyone, everywhere</p>
        </header>
        <div className="gradient-divider shadow-sm"></div>

        <main className="p-2 bg-grey">
          <div className="container">
            <div className="row">
              <div className="col-md-4 my-4">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">Find The Class</h5>
                    <p className="card-text">
                      Simply go to the course page function to locate your
                      className and find further information on location, prices
                      and dates.
                    </p>
                  </div>
                </div>
              </div>

              <div className="col-md-4 my-4">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">Book The Class</h5>
                    <p className="card-text">
                      Read your course overview by clicking on the course and
                      book your place by clicking on the “Enroll Course” button.
                    </p>
                  </div>
                </div>
              </div>

              <div className="col-md-4 my-4">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">Enjoy The Class</h5>
                    <p className="card-text">
                      Once you have booked a course, you now have an access to
                      the high quality training and education the course
                      provides.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <footer className="border-top text-center small text-muted py-3">
          <p className="m-0">
            Zuitter Booking Services | Zuitt Coding Bootcamp &copy; 2020
          </p>
        </footer>
      </>
    );
  }
}

export default Home;
