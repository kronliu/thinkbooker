import React, { Component } from "react";
import OutsideAlerter from "../components/OutsideAlerter";
import NavBar from "../components/NavBar";
import EditCourseForm from "../components/EditCourseForm";
import CreateCourseForm from "../components/CreateCourseForm";
import { Link } from "react-router-dom";
import { Redirect } from "react-router-dom";
import Swal from "sweetalert2";
import { Alert } from "react-bootstrap";
class Courses extends Component {
  state = {
    show: false,
    showCreate: false,
    courses: [],
    selectedCourseId: null,
    willRedirect: false,
    loading: true,
  };

  componentDidMount() {
    fetch("https://intense-waters-48513.herokuapp.com/api/courses/")
      .then((res) => res.json())
      .then((data) => {
        const courses = data;
        this.setState({ courses });
        this.setState({ loading: false });
      });
  }

  handleShow = () => {
    this.setState({ show: true });
  };
  handleClose = () => {
    this.setState({ show: false });
    fetch("https://intense-waters-48513.herokuapp.com/api/courses/")
      .then((res) => res.json())
      .then((data) => {
        const courses = data;
        this.setState({ courses });
        this.setState({ loading: false });
      });
  };

  handleShowCreate = () => {
    this.setState({ showCreate: true });
  };
  handleCloseCreate = () => {
    this.setState({ showCreate: false });
  };

  handleRedirect = () => {
    this.setState({ willRedirect: true });
  };

  handleActive = (courseId) => {
    fetch(
      `https://intense-waters-48513.herokuapp.com/api/courses/${courseId}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        if (data.isActive) {
          fetch(
            `https://intense-waters-48513.herokuapp.com/api/courses/archive/${courseId}`,
            {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
            }
          )
            .then((res) => {
              return res.json();
            })
            .then((data) => {
              if (data) {
                fetch("https://intense-waters-48513.herokuapp.com/api/courses/")
                  .then((res) => res.json())
                  .then((data) => {
                    const courses = data;
                    this.setState({ courses });
                  });
                Swal.fire({
                  icon: "success",
                  title: "Course Archived",
                });
              } else {
                Swal.fire({
                  icon: "error",
                  title: "Something Went Wrong",
                });
              }
            });
        } else {
          fetch(
            `https://intense-waters-48513.herokuapp.com/api/courses/enable/${courseId}`,
            {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
            }
          )
            .then((res) => {
              return res.json();
            })
            .then((data) => {
              if (data) {
                fetch("https://intense-waters-48513.herokuapp.com/api/courses/")
                  .then((res) => res.json())
                  .then((data) => {
                    const courses = data;
                    this.setState({ courses });
                  });
                Swal.fire({
                  icon: "success",
                  title: "Course Enabled",
                });
              } else {
                Swal.fire({
                  icon: "error",
                  title: "Something Went Wrong",
                });
              }
            });
        }
      });
  };

  render() {
    if (this.state.loading) {
      return (
        <div className="loading-screen d-flex flex-column align-items-center justify-content-center background-img">
          <div className="loader"></div>
        </div>
      );
    }
    return this.state.willRedirect ? (
      <Redirect
        to={
          localStorage.getItem("isAdmin") === "false" ? "/profile" : "/courses"
        }
      />
    ) : (
      <>
        <NavBar handleRedirect={this.handleRedirect} />
        <header className="text-center d-flex flex-column align-items-center justify-content-center background-img">
          <h1 className="text-white">Popular Courses</h1>
        </header>
        <div className="divider shadow-sm">
          {localStorage.getItem("isAdmin") === "true" && (
            <button
              className="btn btn-primary"
              id="createCourseBtn"
              onClick={this.handleShowCreate}
            >
              Create Course
            </button>
          )}
        </div>
        <section className="bg-grey py-5">
          <div className="container">
            {this.state.courses.length === 0 && (
              <Alert variant="info" className="alert text-center">
                <Alert.Heading>No Available Courses</Alert.Heading>
              </Alert>
            )}
            <div className="row">
              {this.state.courses
                .filter((course) => {
                  if (
                    localStorage.getItem("isAdmin") === "false" ||
                    localStorage.getItem("isAdmin") === null
                  )
                    return course.isActive === true;
                  return course;
                })
                .map((course) => {
                  return (
                    <div className="col-12 col-md-6" key={course._id}>
                      <div className="card shadow container course-container my-2">
                        <div className="row pt-3 px-3">
                          <div className="col-8">
                            <h4 className="course-name text-dark">
                              {course.name}
                            </h4>
                          </div>

                          <span
                            className={
                              course.isActive
                                ? "card-header-green ml-auto"
                                : "card-header-red ml-auto"
                            }
                          >
                            {course.isActive ? "Active" : "Inactive"}
                          </span>
                        </div>
                        <div className="row px-3">
                          <div className="col-8">
                            <p className="course-description text-dark">
                              {course.description}
                            </p>
                          </div>
                        </div>
                        <div className="row mt-auto mb-3">
                          {localStorage.getItem("isAdmin") === "false" ||
                          localStorage.getItem("isAdmin") === null ? (
                            <Link
                              to={`/course/?courseId=${course._id}`}
                              className=" ml-auto mr-3"
                            >
                              <button className="btn btn-primary" type="button">
                                View Course
                              </button>
                            </Link>
                          ) : (
                            <>
                              <button
                                className={
                                  course.isActive
                                    ? "btn btn-danger ml-auto mr-3"
                                    : "btn btn-success ml-auto mr-3"
                                }
                                data-id={course._id}
                                onClick={(e) => {
                                  this.setState({
                                    selectedCourseId: e.target.getAttribute(
                                      "data-id"
                                    ),
                                  });
                                  this.handleActive(course._id);
                                }}
                              >
                                {course.isActive
                                  ? "Archive Course"
                                  : "Enable Course"}
                              </button>
                              <button
                                className="btn btn-secondary mr-3"
                                data-id={course._id}
                                onClick={(e) => {
                                  this.handleShow();
                                  this.setState({
                                    selectedCourseId: e.target.getAttribute(
                                      "data-id"
                                    ),
                                  });
                                }}
                              >
                                Edit Course
                              </button>
                              {/* <button
                                className="btn btn-primary mr-3"
                                data-id={course._id}
                                onClick={(e) => {
                                  this.setState({
                                    selectedCourseId: e.target.getAttribute(
                                      "data-id"
                                    ),
                                  });
                                }}
                              >
                                View Enrollees
                              </button> */}
                            </>
                          )}
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        </section>

        <div className={this.state.show ? "modal modal-show " : "modal"}>
          <div className="modal-content col-sm-10 col-md-6 p-3">
            {this.state.show && (
              <OutsideAlerter handleClose={this.handleClose}>
                <EditCourseForm
                  courseId={this.state.selectedCourseId}
                  handleClose={this.handleCloseCreate}
                />
              </OutsideAlerter>
            )}
          </div>
        </div>
        <div className={this.state.showCreate ? "modal modal-show " : "modal"}>
          <div className="modal-content col-sm-10 col-md-6 p-3">
            {this.state.showCreate && (
              <OutsideAlerter handleClose={this.handleCloseCreate}>
                <CreateCourseForm
                  courseId={this.state.selectedCourseId}
                  handleClose={this.handleClose}
                />
              </OutsideAlerter>
            )}
          </div>
        </div>
      </>
    );
  }
}

export default Courses;
