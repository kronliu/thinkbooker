import React, { Component } from "react";
import NavBar from "../components/NavBar";
import { Link } from "react-router-dom";
import { Alert } from "react-bootstrap";

class Profile extends Component {
  state = {
    name: "",
    email: "",
    mobileNo: "",
    courses: [],
    loading: "true",
  };
  componentDidMount() {
    fetch("https://intense-waters-48513.herokuapp.com/api/users/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({ loading: false });
        const name = `${data.firstname} ${data.lastname}`;
        const email = data.email;
        const mobileNo = data.mobileNo;
        this.setState({ name, email, mobileNo });

        data.enrollments.forEach((enrollment) => {
          fetch(
            `https://intense-waters-48513.herokuapp.com/api/courses/${enrollment.courseId}`,
            {
              method: "GET",
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
            }
          )
            .then((res) => {
              return res.json();
            })
            .then((data) => {
              let courses = [...this.state.courses];
              courses.push(data);
              this.setState({ courses });
            });
        });
      });
  }

  render() {
    console.log(this.state.loading);
    if (this.state.loading) {
      return (
        <div className="loading-screen d-flex flex-column align-items-center justify-content-center background-img">
          <div className="loader"></div>
        </div>
      );
    }
    return (
      <>
        <NavBar />
        <header className="text-center background-img">
          <div className="col-md-11 offset-md-1 text-center text-md-left mt-auto">
            <h2 id="userName" className="text-white">
              {this.state.name}
            </h2>
          </div>
        </header>
        <div className="divider shadow-sm px-3 text-center text-md-left d-flex flex-row justify-content-md-start justify-content-center">
          <div className="offset-md-1">
            <i className="fa fa-envelope"></i> <span>{this.state.email}</span>
          </div>
          <div>
            <i className="fa fa-phone ml-3"></i>{" "}
            <span>{this.state.mobileNo}</span>
          </div>
        </div>
        <section className="bg-grey py-5">
          <div className="container">
            <h3 className="text-center">Enrolled Courses:</h3>
            <hr className="my-4" />
            {this.state.courses.length === 0 && (
              <Alert variant="info" className="alert text-center">
                <Alert.Heading>No Available Courses</Alert.Heading>
              </Alert>
            )}
            <div className="row">
              {this.state.courses.map((course) => {
                return (
                  <div className="col-12 col-md-6" key={course._id}>
                    <div className="card shadow container course-container my-2">
                      <div className="row flex flex-column p-3">
                        <h4 className="course-name text-dark">{course.name}</h4>
                        <p className="course-description text-dark">
                          {course.description}
                        </p>
                      </div>
                      <div className="row mt-auto mb-3">
                        <Link
                          to={`/course/?courseId=${course._id}`}
                          className=" ml-auto mr-3"
                        >
                          <button className="btn btn-primary" type="button">
                            View Course
                          </button>
                        </Link>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default Profile;
