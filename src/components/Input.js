import React, { Component } from "react";

class Input extends Component {
  state = {};

  addcl = (element) => {
    let parent = element.parentNode;
    parent.classList.add("focus");
  };

  remcl = (element) => {
    let parent = element.parentNode;
    if (element.value === "") {
      parent.classList.remove("focus");
    }
  };

  render() {
    return (
      <div className="form-group input-container">
        <h5>{this.props.title}</h5>
        <input
          type={this.props.type}
          className="form-control input"
          onFocus={(e) => this.addcl(e.target)}
          onBlur={(e) => this.remcl(e.target)}
          onChange={this.props.onChange}
          onClick={this.props.onClick}
        />
      </div>
    );
  }
}

export default Input;
