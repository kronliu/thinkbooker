import React, { Component } from "react";
import { ModalBody, Nav, Navbar } from "react-bootstrap";
import LoginForm from "../components/LoginForm";
import OutsideAlerter from "../components/OutsideAlerter";
import RegisterForm from "../components/RegisterForm";

class NavBar extends Component {
  state = {
    show: false,
    showRegister: false,
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.showRegister || this.state.show) {
      document.querySelector("body").classList.add("overflow");
    } else {
      document.querySelector("body").classList.remove("overflow");
    }
  }

  handleShow = () => {
    this.setState({ show: true });
  };
  handleClose = () => {
    this.setState({ show: false });
  };
  handleShowRegister = () => {
    this.setState({ showRegister: true });
  };
  handleCloseRegister = () => {
    this.setState({ showRegister: false });
  };

  render() {
    return (
      <>
        <Navbar
          expand="lg"
          className="navbar navbar-expand-lg navbar-light shadow"
        >
          <Navbar.Brand href="/">
            <span className="text-teal">think</span>Booker
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              {localStorage.getItem("token") !== null ? (
                <>
                  {localStorage.getItem("isAdmin") === "false" && (
                    <Nav.Link href="/profile">
                      <button className="nav-btn">Profile</button>
                    </Nav.Link>
                  )}
                  <Nav.Link href="/courses">
                    <button className="nav-btn">Courses</button>
                  </Nav.Link>
                  <Nav.Link href="/logout">
                    <button className="nav-btn">Logout</button>
                  </Nav.Link>
                </>
              ) : (
                <>
                  <Nav.Link href="/courses">
                    <button className="nav-btn">Courses</button>
                  </Nav.Link>
                  <Nav.Link onClick={this.handleShowRegister}>
                    <button className="nav-btn">Register</button>
                  </Nav.Link>

                  <Nav.Link onClick={this.handleShow}>
                    <button className="login-button">Login </button>
                  </Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <div className="gradient-divider shadow-sm"></div>

        <div className={this.state.show ? "modal modal-show " : "modal"}>
          <div className="modal-content col-sm-10 col-md-6">
            {this.state.show && (
              <OutsideAlerter handleClose={this.handleClose}>
                <LoginForm handleRedirect={this.props.handleRedirect} />
              </OutsideAlerter>
            )}
          </div>
        </div>

        <div
          className={this.state.showRegister ? "modal modal-show " : "modal"}
        >
          <div className="modal-content col-sm-10 col-md-6">
            {this.state.showRegister && (
              <OutsideAlerter handleClose={this.handleCloseRegister}>
                <RegisterForm />
              </OutsideAlerter>
            )}
          </div>
        </div>
      </>
    );
  }
}

export default NavBar;
