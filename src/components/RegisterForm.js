import React, { Component } from "react";
import Input from "../components/Input";
import Swal from "sweetalert2";
import { Alert } from "react-bootstrap";

class RegisterForm extends Component {
  state = {
    email: "",
    firstname: "",
    lastname: "",
    password: "",
    password2: "",
    mobileNo: null,
    alertMessage: null,
    loading: false,
  };

  handleRegister = (e) => {
    e.preventDefault();
    const {
      email,
      firstname,
      lastname,
      password,
      password2,
      mobileNo,
    } = this.state;
    this.setState({ loading: true });
    if (
      email !== "" &&
      firstname !== "" &&
      lastname !== "" &&
      password !== "" &&
      password2 !== "" &&
      password === password2 &&
      mobileNo.length === 11
    ) {
      fetch(
        "https://intense-waters-48513.herokuapp.com/api/users/email-exists",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: email,
          }),
        }
      )
        .then((response) => response.json())
        .then((data) => {
          if (data === false) {
            fetch("https://intense-waters-48513.herokuapp.com/api/users", {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password,
                mobileNo: mobileNo,
              }),
            })
              .then((response) => response.json())
              .then((data) => {
                if (data) {
                  // Swal.fire({
                  //   icon: "success",
                  //   title: "Registration Successful",
                  // });
                  this.setState({ alertMessage: "success" });
                  this.setState({ loading: false });
                } else {
                  // Swal.fire({
                  //   icon: "error",
                  //   title: "Registration Failed",
                  // });
                  this.setState({ alertMessage: "error" });
                  this.setState({ loading: false });
                }
              });
          } else {
            // Swal.fire({
            //   icon: "error",
            //   title: "Email Already Exists",
            // });
            this.setState({ alertMessage: "email-exists" });
            this.setState({ loading: false });
          }
        });
    } else {
      this.setState({ alertMessage: "invalid-input" });
      this.setState({ loading: false });
    }

    this.setState({
      email: "",
      firstname: "",
      lastname: "",
      password: "",
      password2: "",
      mobileNo: null,
    });
  };

  handleAlertShow = () => {
    if (this.state.alertMessage === "success")
      return (
        <Alert variant="success" className="alert text-center">
          <Alert.Heading>Registration Successful</Alert.Heading>
        </Alert>
      );
    if (this.state.alertMessage === "error")
      return (
        <Alert variant="danger" className="alert text-center">
          <Alert.Heading>Unable to create an account.</Alert.Heading>
        </Alert>
      );
    if (this.state.alertMessage === "email-exists")
      return (
        <Alert variant="warning" className="alert text-center">
          <Alert.Heading>Email already exists.</Alert.Heading>
        </Alert>
      );
    if (this.state.alertMessage === "invalid-input")
      return (
        <Alert variant="danger" className="alert text-center">
          <Alert.Heading>Invalid input.</Alert.Heading>
        </Alert>
      );
    return null;
  };

  render() {
    return (
      <div className="form-container col-sm-10 col-md-8 mx-auto">
        <form>
          <h2 className="title">Welcome</h2>
          {this.handleAlertShow()}
          {!this.state.loading ? (
            <>
              <div className="row">
                <div className="col-6">
                  <Input
                    title={"First Name"}
                    type={"text"}
                    onChange={(e) =>
                      this.setState({ firstname: e.target.value })
                    }
                    onClick={(e) => this.setState({ alertMessage: null })}
                  />
                </div>
                <div className="col-6">
                  <Input
                    title={"Last Name"}
                    type={"text"}
                    onChange={(e) =>
                      this.setState({ lastname: e.target.value })
                    }
                    onClick={(e) => this.setState({ alertMessage: null })}
                  />
                </div>
              </div>
              <Input
                title={"Email"}
                type={"email"}
                onChange={(e) => this.setState({ email: e.target.value })}
                onClick={(e) => this.setState({ alertMessage: null })}
              />
              <Input
                title={"Password"}
                type={"password"}
                onChange={(e) => this.setState({ password: e.target.value })}
                onClick={(e) => this.setState({ alertMessage: null })}
              />
              <Input
                title={"Verify Password"}
                type={"password"}
                onChange={(e) => this.setState({ password2: e.target.value })}
                onClick={(e) => this.setState({ alertMessage: null })}
              />
              <Input
                title={"Mobile Number"}
                type={"Number"}
                onChange={(e) => this.setState({ mobileNo: e.target.value })}
                onClick={(e) => this.setState({ alertMessage: null })}
              />
            </>
          ) : (
            <div className="loader-container">
              <div className="loader"></div>
            </div>
          )}
          <button
            type="submit"
            className="btn btn-primary"
            onClick={(e) => {
              this.handleRegister(e);
            }}
          >
            Register
          </button>
        </form>
      </div>
    );
  }
}

export default RegisterForm;
