import React, { Component } from "react";
import Input from "../components/Input";
import Swal from "sweetalert2";
import { Alert } from "react-bootstrap";

class LoginForm extends Component {
  state = {
    email: "",
    password: "",
    alertMessage: null,
    loading: false,
  };

  handleLogin = (e) => {
    e.preventDefault();
    console.log(document.querySelector("body"));
    this.setState({ loading: true });
    const email = this.state.email;
    const password = this.state.password;
    if (email === "" || password === "") {
      // Swal.fire({
      //   icon: "error",
      //   title: "Invalid Input",
      // });
      this.setState({ alertMessage: "invalid-input" });
      this.setState({ loading: false });
    } else {
      fetch("https://intense-waters-48513.herokuapp.com/api/users/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          console.log(data);
          if (data.access !== null && data !== false) {
            localStorage.setItem("token", data.access);
            fetch(
              "https://intense-waters-48513.herokuapp.com/api/users/details",
              {
                headers: {
                  Authorization: `Bearer ${data.access}`,
                },
              }
            )
              .then((res) => {
                return res.json();
              })
              .then((data) => {
                localStorage.setItem("isAdmin", data.isAdmin);
                this.setState({ loading: false });
                this.props.handleRedirect();
              });
          } else {
            // Swal.fire({
            //   icon: "error",
            //   title: "Password might be incorrect or user does not exists.",
            // });
            this.setState({ alertMessage: "error" });
            this.setState({ loading: false });
          }
        });
    }
    this.setState({ email: "", password: "" });
  };

  handleAlertShow = () => {
    if (this.state.alertMessage === "error")
      return (
        <Alert variant="danger" className="alert text-center">
          <Alert.Heading>
            Password might be incorrect or user does not exists.
          </Alert.Heading>
        </Alert>
      );
    if (this.state.alertMessage === "invalid-input")
      return (
        <Alert variant="danger" className="alert text-center">
          <Alert.Heading>Invalid input.</Alert.Heading>
        </Alert>
      );
    return null;
  };

  render() {
    return (
      <div className="form-container col-sm-10 col-md-8 mx-auto">
        <form>
          <h2 className="title">Welcome</h2>
          {this.handleAlertShow()}
          {!this.state.loading ? (
            <>
              <Input
                title={"Email"}
                type={"text"}
                onChange={(e) => {
                  this.setState({ email: e.target.value });
                }}
                onClick={(e) => this.setState({ alertMessage: null })}
              />
              <Input
                title={"Password"}
                type={"password"}
                onChange={(e) => {
                  this.setState({ password: e.target.value });
                }}
                onClick={(e) => this.setState({ alertMessage: null })}
              />
            </>
          ) : (
            <div className="loader-container">
              <div className="loader"></div>
            </div>
          )}

          <button
            type="submit"
            className="btn btn-primary"
            onClick={(e) => {
              this.handleLogin(e);
            }}
          >
            Login
          </button>
        </form>
      </div>
    );
  }
}

export default LoginForm;
