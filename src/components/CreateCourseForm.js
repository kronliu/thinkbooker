import React, { Component } from "react";
import Input from "../components/Input";
import Swal from "sweetalert2";

class CreateCourseForm extends Component {
  state = {
    name: "",
    description: "",
    price: 0,
  };

  handleSubmit(e) {
    e.preventDefault();
    const { name, description, price } = this.state;
    if (name !== "" && description !== "" && price > 0) {
      console.log("passed");
      fetch("https://intense-waters-48513.herokuapp.com/api/courses", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          name: name,
          description: description,
          price: price,
        }),
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          if (data) {
            Swal.fire({
              icon: "success",
              title: "Course Created",
            });
            this.props.handleClose();
          } else {
            Swal.fire({
              icon: "error",
              title: "Unable to Create Course",
            });
          }
        });
    } else {
      Swal.fire({
        icon: "error",
        title: "Invalid Input",
      });
    }
  }

  render() {
    return (
      <div className="form-container col-sm-10 col-md-8 mx-auto">
        <form onSubmit={(e) => this.handleSubmit(e)}>
          <h2 className="mb-4">Create Course</h2>
          <Input
            title={"Name"}
            type={"text"}
            onChange={(e) => {
              this.setState({ name: e.target.value });
            }}
          />
          <Input
            title={"Description"}
            type={"text"}
            onChange={(e) => {
              this.setState({ description: e.target.value });
            }}
          />
          <Input
            title={"Price"}
            type={"number"}
            onChange={(e) => {
              this.setState({ price: e.target.value });
            }}
          />
          <button type="submit" className="btn btn-primary">
            Create Course
          </button>
        </form>
      </div>
    );
  }
}

export default CreateCourseForm;
